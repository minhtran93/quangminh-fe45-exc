//HIỂN THỊ
const postDataProducts = ()=>{
    const id = document.getElementById('idProducts').value;
    const name = document.getElementById('nameProducts').value;
    const img = document.getElementById('linkImage').value;
    const description = document.getElementById('descProduct').value;
    const price = document.getElementById('priceProduct').value;
    const rating = document.getElementById('ratingProducts').value;
    const inventory = document.getElementById('inventProduct').value;
    const type = document.getElementById('typeProduct').value.replace(/\s/g, '');


    //Check required
    checkRequiredPro('nameProducts','nameErrPro','Nhập tên sản phẩm *');
    checkRequiredPro('linkImage','imgErrPro','Nhập link hình ảnh *');
    checkRequiredPro('descProduct','descErrPro','Nhập mô tả sản phẩm *');
    checkRequiredPro('typeProduct','typeErrPro','Nhập loại sản phẩm *');
    checkRequiredPro('priceProduct','priceErrPro','Nhập giá sản phẩm *');
    checkRequiredPro('inventProduct','inventErrPro','Nhập số lượng tồn kho *');


    const checkTextError = document.getElementsByClassName('textError').value.trim().toLowerCase();
    if(checkTextError = ''){
        alert('Vui lòng điền thông tin sản phẩm');
        return false;
    } else{
        const product = new SanPham (    
            id,
            name,
            img,
            description,
            price,
            inventory,
            rating,
            type);
            updateProducts(product).then(function(result){
                fetchProduct();
            });
    }
}

//Tạo mảng danh sách SP
let productsList = [];
const fetchProduct = () => {
    getproductsList().then(function(result){
        productsList = result.data;
        renderHTML();
    });
}
fetchProduct();

//Render HTML
const renderHTML = () => {
    let htmlContent = "";
    for (let i in productsList){
        let product = productsList[i];
        htmlContent += `
        <tr>
        <td>${product.id}</td>
        <td>${product.name}</td>
        <td><img style="width:50px;" src="${product.img}" /></td>
        <td style='display:none'>${product.description}</td>
        <td>${product.price}</td>
        <td>${product.inventory}</td>
        <td style='display:none'>${product.rating}</td>
        <td>${product.type}</td>
        <td>
            <button class="btn btn-info rounded-circle" onClick="getUpdatePro('${product.id}')">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </button>

          <button class="btn btn-danger" onclick="deleteProduct(${product.id})">Xoá</button>
        </tr>
    `;
    }
    document.getElementById("tbodyEmp").innerHTML = htmlContent;
}

//Xóa người dùng
const deleteProduct = (id) => {
    deleteProducts(id).then(function(){
        fetchProduct();
    });
}

//Uptdate thông tin
//1. FindById
const findProById = (id)=>{
    for(let index in productsList ){
        if(productsList[index].id === id){
            return index;
        }
    }
    return -1;
}
//2. Get thông tin cũ
const getUpdatePro = (id)=>{
    getProducts(id).then(function(result){
            const newUpdatePro = result.data;
            document.getElementById('idProducts').value = newUpdatePro.id; 
            document.getElementById('nameProducts').value = newUpdatePro.name; 
            document.getElementById('linkImage').value = newUpdatePro.img; 
            document.getElementById('descProduct').value = newUpdatePro.description;
            document.getElementById('priceProduct').value = newUpdatePro.price;
            document.getElementById('inventProduct').value = newUpdatePro.inventory;
            document.getElementById('ratingProducts').value = newUpdatePro.rating;
            document.getElementById('typeProduct').value = newUpdatePro.type;
    
            //disable name,type
            document.getElementById('nameProducts').setAttribute('disabled',true);
            document.getElementById('typeProduct').setAttribute('disabled',true);
    
            //Ẩn nút button Thêm Nhân Viên, đổi thành xóa
            document.getElementById('btnAdd').style.display = 'none';
            document.getElementById('btnUpdate').style.display = 'block';
    });
}
//3. Sửa thông tin
const updatePro =()=>{
    const id = document.getElementById('idProducts').value;
    const name = document.getElementById('nameProducts').value;
    const img = document.getElementById('linkImage').value;
    const description = document.getElementById('descProduct').value;
    const price = document.getElementById('priceProduct').value;
    const inventory = document.getElementById('inventProduct').value;
    const rating = document.getElementById('ratingProducts').value;
    const type = document.getElementById('typeProduct').value;

    const newUpdatePro = new SanPham (
        id,
        name,
        img,
        description,
        price,
        inventory,
        rating,
        type
    );
    updatePros(id, newUpdatePro).then(function(){
        fetchProduct();
        renderHTML();
    })

    //sau khi update thì remove disable
    document.getElementById('nameProducts').removeAttribute('disabled');
    document.getElementById('typeProduct').removeAttribute('disabled');

    //saukhi update thì hiện lại nút add;
    document.getElementById('btnAdd').style.display = 'block';
    document.getElementById('btnUpdate').style.display = 'none';
    //trigger reset
    document.getElementById('btnReset').click();
}

//check required
const checkRequiredPro = (id,errorId,message)=> {
    const value = document.getElementById(id).value;
    if(value){
        document.getElementById(errorId).innerHTML = '';
        return true;
    }
    document.getElementById(errorId).innerHTML = message;
    return false;
};
