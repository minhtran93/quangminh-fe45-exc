function SanPham (
    id,
    img,
    name,
    description,
    price,
    inventory,
    rating,
    type
){
    this.id = id;
    this.img = img;
    this.name = name;
    this.description = description;
    this.price = price;
    this.inventory = inventory;
    this.rating = rating;
    this.type = type;
}