var productsList = [];

//Câu 1.1
//Lấy dữ liệu từ data
function fetchProductList() {
  getproductsList()
    .then(function (res) {
      // console.log(res);
      mapData(res.data);
      //  productsList = result.data;
      renderHTML();
    })
    .catch(function (err) {
      // console.log(err);
    });
}
//Mapdata
const mapData = (data = []) => {
  for (let item of data) {
    let myProductObject;
    myProductObject = new SanPham(
      item.id,
      item.img,
      item.name,
      item.description,
      item.price,
      item.inventory,
      item.rating,
      item.type
    );
    productsList.push(myProductObject);
  }

  // console.log(productsList);
};
//Hiển thị giao diện
const renderHTML = (arr) => {
  arr = arr || productsList;
  var htmlContent = "";
  for (var i = 0; i < arr.length; i++) {
    var products = arr[i];
    htmlContent += `
      <div class="card p-2 col-4">
      <img style="height:280px;" src="${products.img}" class="w-100" alt="">
      <p class="d-none" id="idProducts-${products.id}">${products.id}</p>
      <h3 class="text-center">${products.name}</h3>
      <p>
        ${products.description}
      </p>
      <p>Giá thành: ${products.price}</p>
      <button class="btn btn-success" onclick="addProductsCart(${products.id})" id="addToCart">Thêm Giỏ Hàng</button>
    </div>
      `;
  }
  document.getElementById("divProducts").innerHTML = htmlContent;
};

//Tìm kiếm sản phẩm thông qua id
const findProductsById = (id) => {
  for (let i in this.productsList) {
    const products = parseInt(productsList[i].id);
    if (products === id) {
      return i;
    }
  }
  return -1;
};

//Câu 1.2: Sort Name
const sortName = (type) => {
  if (type === 1) {
    let arr;
    arr = productsList.sort((a, b) => {
      let x = a.name.toLowerCase().trim();
      let y = b.name.toLowerCase().trim();
      if (x > y) {
        return 1;
      }
      if (x < y) {
        return -1;
      }
      return 0;
    });
    console.log(arr);
    renderHTML(arr);
  } else {
    let arr;
    arr = productsList.sort((a, b) => {
      let x = a.name.toLowerCase().trim();
      let y = b.name.toLowerCase().trim();
      if (x > y) {
        return -1;
      }
      if (x < y) {
        return 1;
      }
      return 0;
    });
    console.log(arr);
    renderHTML(arr);
  }
};
sortName();
// console.log(sortName);
// Câu 2: Filter Sort Type

const filterType = () => {
  let keyword = document.getElementById("filterType").value.toLowerCase();
  let filterTypeList = [];
  filterTypeList = productsList.filter((Object) => Object.type === keyword);
  console.log(filterTypeList);
  renderHTML(filterTypeList);
};
// console.log(filterType());
// const proIphone = productsList.filter((hang) => hang.type === 'samsung');
// console.log(proIphone);

//ADD Product
let productsCart = [];
var newCartList;
let CartList = [];
const addProductsCart = (id) => {
  const checkID = findProductsById(id);
  if (checkID !== -1) {
    productsCart = productsList[checkID];
    newCartList = { ...productsCart, quantity: 1 };
    for (let i in CartList) {
      if (CartList[i].id === newCartList.id) {
        CartList[i].quantity++;
        SumMoney();
        renderCart();
        saveData();
        return;
      }
    }
    CartList.push(newCartList);
    productsCart = CartList;
  }
  console.log(productsCart);
  SumMoney();
  renderCart();
  saveData();
};

//Thêm số lượng SP trong giỏ hàng
const addQuantity = (idCart) => {
  const checkID = findCartId(idCart);
  if (checkID !== -1) {
    CartList[checkID].quantity+=1;
    saveData();
    renderCart();
  }
  SumMoney();
  renderCart();
  saveData();
};
//Giảm số lượng SP trong giỏ hàng
const subSQuantity = (idCart) => {
  const checkID = findCartId(idCart);
  if (checkID !== -1) {
    if(CartList[checkID].quantity >= 1){
      CartList[checkID].quantity-= 1;
      SumMoney();
      saveData();
      renderCart();
      return
    };
  };
  SumMoney();
  renderCart();
  saveData();
}


let renderCart = (arr) => {
  arr = arr || CartList;
  var htmlContentCart = "";
  for (var item = 0; item < arr.length;item++) {
    var renProCart = arr[item];
    htmlContentCart += `
      <tr>
        <td><img style="width:50px;" src="${renProCart.img}" /></td>
        <td style="font-size:25px;">${renProCart.name}</td>
        <td>${renProCart.price}$</td>
        <td>
          ${renProCart.quantity}
        <div class="btn-group">
          <button class="btn btn-info border-right" onclick="subSQuantity(${renProCart.id})">-</button>
          <button class="btn btn-info border-left" onclick="addQuantity(${renProCart.id})">+</button>
        </div>
        </td>
        <td id="price">${renProCart.price * renProCart.quantity}</td>
        <td>
          <button class="btn btn-info" onclick="deletePro(${renProCart.id})">x</button>
        </td>
      </tr>
    `;
  }
  document.getElementById('renderCart').innerHTML = htmlContentCart;
};
fetchProductList();


//Tìm kiếm Id SP trong giỏ hàng
const findCartId = (idCart) => {
  for (let i in CartList) {
    const productsCartItem = parseInt(CartList[i].id);
    // const soID = parseInt(idCart)
    if (productsCartItem === idCart) {
       return i;
    }
    // console.log(CartList[i].id);
  }
  return -1;
};
const deletePro = (idCart) => {
  const checkID = findCartId(idCart);
  if (checkID !== -1) {
    CartList.splice(checkID, 1);
    SumMoney();
    saveData();
    renderCart();
  }
console.log(CartList)
};
// hàm clear giỏ hàng

const ClearCart = () => {
  for(let item of CartList){
    if(item === item){
      CartList = [];
      SumMoney();
      saveData();
      renderCart(CartList);
      console.log(CartList);
    }
  }
}
// //Tổng tiền thanh toán
const SumMoney = () => {
  let money;
  let sum = 0;
  for (let item of CartList){
    if(CartList.length > 0){
      money = item.price * item.quantity;
      sum = sum + money;
      // sum1 = sum;
      console.log(item);
    }
    // return sum;
  }
    document.getElementById('sumProductPaid').innerHTML = sum;  
    return sum;
}
// const SumMoney = () => {
//   let sum=0;
//   let sumPro;
//   for(let item of CartList){
//     if(CartList.length>0 ){
//       sumPro = item.price * item.quantity;
//       sum += sumPro;
//       console.log(item);
//     };
//   };
//   document.getElementById('sumProductPaid').innerHTML = sum;
//   return sum;
//   // console.log(sum);
// }

//Save Data Cart
const saveData = () =>{
  localStorage.setItem('CartList', JSON.stringify(CartList));
};

const loadData = ()=> {
  var proCartJson = localStorage.getItem('CartList');
  if(!proCartJson){
    return;
  }
  const proCartListFromLocal = JSON.parse(proCartJson);
  for(var index = 0; index < proCartListFromLocal.length; index++){
    const currentPro = proCartListFromLocal[index];
    const NewPro = new SanPham(
      currentPro.id,
      currentPro.img,
      currentPro.name,
      currentPro.description,
      currentPro.price,
      currentPro.inventory,
      currentPro.rating,
      currentPro.type,
    );

       let NewProList = {...NewPro, quantity: 1};
      CartList.push(NewProList);
  }
  renderCart();
};
loadData();
// const loadData = ()=> {
